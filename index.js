console.log("Hello World");


let trainer = {};

trainer.name = "Brock",
trainer.age = 20,
trainer.pokemon = ["Venosaur","Charizard","Pickachu","Togepi"],
trainer.friends = ["Ash Ketchum","koga","May"],
trainer.talk = function(){
		console.log("Pickachu!, I choose you");
	}

console.log(trainer);

console.log("Result of Dot Notation.");
console.log(trainer.name);

console.log("Result of Bracket Notation.");
console.log(trainer.pokemon);



console.log("Result of Talk Method");
trainer.talk();



function Pokemon(name,level,health,attack){
	
	this.name = name;
	this.level = level;
	this.health = health*(3*level);
	this.attack = attack*(1.5*level);
	this.tackle = function tackle(enemy){
		console.log(name + " tackled " +enemy.name)
		original_heal = enemy.health
		enemy.health = enemy.health-this.attack
		console.log(enemy.name + " has received " + this.attack + " attack damage from " + this.name)
		console.log(enemy.name + "'s health has been reduced from "+ original_heal + " to " + enemy.health)
		if (enemy.health <=0){
			this.faint()
		}
	};
	this.faint = function faint(){
		console.log(name + " has fainted.")
	};
}

let pokemon1 = new Pokemon("Pickachu", 12,1,1);
console.log(pokemon1);

let pokemon2 = new Pokemon("Charizard", 11,1,1);
console.log(pokemon2);

let pokemon3 = new Pokemon("Venosaur", 11,1,1);
console.log(pokemon3);

let pokemon4 = new Pokemon("Togepi", 8,1,1);
console.log(pokemon4);

let pokemon5 = new Pokemon("Mr.Mime", 10,1,1);
console.log(pokemon5);

pokemon1.tackle(pokemon2);
pokemon2.tackle(pokemon1);
pokemon1.tackle(pokemon2);